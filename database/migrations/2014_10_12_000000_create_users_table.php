<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('api_token', 80)->unique()->nullable()->default(null);
            $table->integer('chips')->default(1000)->nullable(false);
            $table->rememberToken();
            $table->timestamps();
        });
        
        \App\User::create([
            'name' => 'Vladimirs',
            'email' => 'budkinvladimir@gmail.com',
            'password' => '$2y$10$Ak22SlkWi.MUhqIUhmP8yePp5G/fZL2/BDlOuFDa9T99HxgObVUTy',
            'api_token' => \Illuminate\Support\Str::random(60)
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
