<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiveCardPokerGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('five_card_poker_games', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->json('user_cards');
            $table->json('dealer_cards');
            $table->json('cards_left');
            $table->unsignedSmallInteger('ante');
            $table->unsignedTinyInteger('isBet')->nullable();
            $table->json('user_changed_cards')->nullable();
            $table->json('dealer_changed_cards')->nullable();
            $table->unsignedTinyInteger('isPayout')->nullable();
            $table->unsignedTinyInteger('combination')->nullable();
            $table->timestamps();
            $table->index(['isPayout']);
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('five_card_poker_games');
    }
}
