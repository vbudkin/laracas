<?php

namespace App\Http\Controllers\Games;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\FiveCardPokerGame;
use Illuminate\Http\Request;
use App\Core\Games\FiveCardPokerGame as Game;

class FiveCardPokerApiController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        $user = Auth::user();
        $lastGame = $user->fiveCardPokerGames()->unfinished()->first();
        $game = $lastGame ?
                new Game($lastGame, $user) :
                new Game(new FiveCardPokerGame(), $user);
        return $game->getFrontendData();
    }

    public function ante(Request $request) {
        $user = Auth::user();
        $request->request->add(['balance' => $user->chips]);
        $ante = $request->ante;
        $request->validate([
            'ante' => [
                'required',
                'integer',
                'between:5,500'
            ],
            'balance' => [
                'integer',
                "gte:{$ante}"
            ]
        ]);
        $game = new Game(new FiveCardPokerGame(), $user);
        $game->ante($ante);
        return $game->getFrontendData();
    }

    public function action($id, $action, Request $request) {
        $user = Auth::user();
        $gameData = $user->fiveCardPokerGames()->unfinished()->findOrFail($id);
        //validation
        $request->request->add(['balance' => $user->chips]);
        $validationRules = [];
        switch ($action) {
            case 'bet':
                $bet = $gameData->ante * 2;
                $validationRules['balance'] = ['integer', "gte:{$bet}"];
                break;
            case 'change':
                $changeCost = $gameData->ante * count($request->cards);
                $validationRules['cards'] = ['required', 'array'];
                $validationRules['balance'] = ['integer', "gte:{$changeCost}"];
                break;
            case 'change-dealer':
                $changeCost = $gameData->ante;
                $validationRules['balance'] = ['integer', "gte:{$changeCost}"];
                break;
        }
        if($validationRules){
            $request->validate($validationRules);
        }
        $game = new Game($gameData, $user);
        //fire game action
        switch ($action) {
            case 'bet':
                $game->bet();
                break;
            case 'change':
                $game->change($request->cards);
                break;
            case 'payout':
                $game->payout();
                break;
            case 'drop':
                $game->drop();
                break;
             case 'change-dealer':
                $game->changeDealer();
                break;
        }
        return $game->getFrontendData();
    }
    
}
