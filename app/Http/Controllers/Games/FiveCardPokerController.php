<?php

namespace App\Http\Controllers\Games;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\FiveCardPokerGame;
use App\Core\Cards\Deck;
use App\Core\Cards\Hand;

class FiveCardPokerController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('games.five-card-poker');
    }

}
