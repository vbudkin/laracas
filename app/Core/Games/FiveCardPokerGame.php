<?php

namespace App\Core\Games;

use Illuminate\Support\Facades\Auth;
use App\FiveCardPokerGame as GameModel;
use App\Core\Cards\Deck;
use App\Core\Cards\Hand;
use App\User;

class FiveCardPokerGame {

    protected $game, $user, $deck;
    protected $dealerCards, $userCards, $userCardsToChange = [];
    protected $payout, $dealerCombination, $userCombination = null;
    protected $dealerCanChange, $userCanChange = false;
    protected $stage = 'ante';
    protected $ante = 5;
    protected $totalBet = 0;

    public function __construct(GameModel $game, User $user) {
        $this->user = $user;
        $this->game = $game;
        $this->deck = new Deck();
        $this->parse();
    }

    protected function parse() {
        $this->game->ante = $this->game->ante ?: 5;
        //new game or last game finished
        if (is_null($this->game->id) || !is_null($this->game->isPayout)) {
            return;
        }
        $this->totalBet = $this->game->ante;
        $playerHand = new Hand($this->game->user_cards);
        $dealerHand = new Hand($this->game->dealer_cards);
        $playerHand->orderCards();
        $this->userCards = $playerHand->cards;
        $this->userCombination = $playerHand->combination;
        $isUserChangedCards = !is_null($this->game->user_changed_cards);
        $this->userCanChange = !$isUserChangedCards;
        if ($isUserChangedCards) {
            $cardsChanged = count(array_diff($this->game->user_cards, $this->game->user_changed_cards));
            $this->totalBet += $this->game->ante * $cardsChanged;
        }
        if (is_null($this->game->isBet)) {
            $this->stage = 'bet';
            $this->dealerCards = [$dealerHand->cards[0], '', '', '', ''];
            return;
        }
        $this->userCanChange = false;
        $this->totalBet += $this->game->ante * 2;
        $this->stage = 'payout';
        $this->dealerCards = $dealerHand->cards;
        $this->dealerCombination = $dealerHand->combination;
        $isUserWin = $this->isUserWin($playerHand, $dealerHand);
        $dealerChangedCard = !is_null($this->game->dealer_changed_cards);
        if ($dealerChangedCard) {
            $this->totalBet += $this->game->ante;
        }
        $this->payout = $this->getPayout($isUserWin, $this->game->ante, $dealerHand->rank, $playerHand->rank, $dealerChangedCard);
        $this->dealerCanChange = $isUserWin && !$dealerChangedCard && ($dealerHand->rank == 0);
    }

    public function ante($ante) {
        $this->deck->makeDeck();
        $this->deck->shuffle();
        $playerCards = $this->deck->take(5);
        $dealerCards = $this->deck->take(5);
        $cardsLeft = $this->deck->getDeck();
        $playerHand = new Hand($playerCards);
        $playerHand->orderCards();
        $dealerHand = new Hand($dealerCards);

        $this->game->user_id = $this->user->id;
        $this->game->user_cards = $playerHand->cards;
        $this->game->dealer_cards = $dealerHand->cards;
        $this->game->cards_left = $cardsLeft;
        $this->game->ante = $ante;
        $this->game->combination = $playerHand->rank;
        $this->game->save();

        $this->user->chips -= $ante;
        $this->user->save();

        $this->userCards = $playerHand->cards;
        $this->userCombination = $playerHand->combination;
        $this->userCanChange = true;
        $this->dealerCards = [$dealerHand->cards[0], '', '', '', ''];
        $this->stage = 'bet';
        $this->totalBet = $ante;
    }

    public function bet() {
        if (is_null($this->game->isBet)) {
            $this->game->isBet = 1;
            $this->game->save();

            $this->user->chips -= $this->game->ante * 2;
            $this->user->save();
        }

        $dealerHand = new Hand($this->game->dealer_cards);
        $playerHand = new Hand($this->game->user_cards);
        $playerHand->orderCards();

        $isUserWin = $this->isUserWin($playerHand, $dealerHand);
        $this->payout = $this->getPayout($isUserWin, $this->game->ante, $dealerHand->rank, $playerHand->rank, false);

        $this->dealerCards = $dealerHand->cards;
        $this->dealerCombination = $dealerHand->combination;
        $this->dealerCanChange = $isUserWin && ($dealerHand->rank == 0);

        $this->userCards = $playerHand->cards;
        $this->userCombination = $playerHand->combination;
        $this->userCanChange = false;

        $this->stage = 'payout';
        $this->totalBet = $this->game->ante + $this->game->ante * 2;
        $isUserChangedCards = !is_null($this->game->user_changed_cards);
        if ($isUserChangedCards) {
            $cardsChanged = count(array_diff($this->game->user_cards, $this->game->user_changed_cards));
            $this->totalBet += $this->game->ante * $cardsChanged;
        }
    }

    public function payout() {
        $dealerHand = new Hand($this->game->dealer_cards);
        $playerHand = new Hand($this->game->user_cards);
        $isUserWin = $this->isUserWin($playerHand, $dealerHand);
        $dealerChangedCard = !is_null($this->game->dealer_changed_cards);
        $payout = $this->getPayout($isUserWin, $this->game->ante, $dealerHand->rank, $playerHand->rank, $dealerChangedCard);
        if (is_null($this->game->isPayout)) {
            $this->game->isPayout = $payout > 0 ? 1 : 0;
            $this->game->save();

            $this->user->chips += $payout;
            $this->user->save();
        }

        $this->dealerCards = [];
        $this->dealerCombination = null;
        $this->dealerCanChange = false;

        $this->userCards = [];
        $this->userCombination = null;
        $this->userCanChange = false;
        $this->userCardsToChange = [];

        $this->stage = 'ante';
        $this->game->id = null;
        $this->totalBet = 0;
        $this->payout = null;
    }

    public function drop() {
        $this->game->isPayout = 0;
        $this->game->isBet = 0;
        $this->game->save();

        $this->dealerCards = [];
        $this->dealerCombination = null;
        $this->dealerCanChange = false;

        $this->userCards = [];
        $this->userCombination = null;
        $this->userCanChange = false;
        $this->userCardsToChange = [];

        $this->stage = 'ante';
        $this->game->id = null;
        $this->totalBet = 0;
        $this->payout = null;
    }

    public function change(array $cardsToChange) {
        $this->deck->setDeck($this->game->cards_left);
        $userCards = $this->game->user_cards;
        foreach ($userCards as $key => $value) {
            if (in_array($value, $cardsToChange)) {
                $userCards[$key] = $this->deck->take(1);
            }
        }
        $cntChangedCards = count($cardsToChange);
        $playerHand = new Hand($userCards);
        if (is_null($this->game->user_changed_cards)) {
            $this->game->user_changed_cards = $this->game->user_cards;
            $this->game->user_cards = $userCards;
            $this->game->cards_left = $this->deck->getDeck();
            $this->game->combination = $playerHand->rank;
            $this->game->save();

            $this->user->chips -= $this->game->ante * $cntChangedCards;
            $this->user->save();
        }
        $this->userCards = $playerHand->cards;
        $this->userCombination = $playerHand->combination;
        $this->userCanChange = false;
        $this->userCardsToChange = [];
        $this->totalBet = $this->game->ante + $this->game->ante * $cntChangedCards;
    }

    public function changeDealer() {
        $this->deck->setDeck($this->game->cards_left);
        $newDealerCard = $this->deck->take(1);
        $dealerHand = new Hand($this->game->dealer_cards);
        $dealerHand->orderCards(false);
        $dealerCards = $dealerHand->cards;
        $dealerCards[0] = $newDealerCard;
        if (is_null($this->game->dealer_changed_cards)) {
            $this->game->cards_left = $this->deck->getDeck();
            $this->game->dealer_changed_cards = $this->game->dealer_cards;
            $this->game->dealer_cards = $dealerCards;
            $this->game->save();

            $this->user->chips -= $this->game->ante;
            $this->user->save();
        }
        $dealerHand = new Hand($dealerCards);
        $playerHand = new Hand($this->game->user_cards);
        $playerHand->orderCards();
        $isUserWin = $this->isUserWin($playerHand, $dealerHand);
        $payout = $this->getPayout($isUserWin, $this->game->ante, $dealerHand->rank, $playerHand->rank, true);

        $this->dealerCards = $dealerHand->cards;
        $this->dealerCombination = $dealerHand->combination;
        $this->dealerCanChange = false;
        $this->payout = $payout;
        $this->totalBet += $this->game->ante;
    }

    public function getFrontendData() {
        $return = [
            'stage' => $this->stage,
            'ante' => $this->game->ante,
            'id' => $this->game->id,
            'totalBet' => $this->totalBet,
            'balance' => $this->user->chips,
            'payout' => $this->payout,
            'dealer' => [
                'cards' => $this->dealerCards,
                'combination' => $this->dealerCombination,
                'canChange' => $this->dealerCanChange
            ],
            'user' => [
                'cards' => $this->userCards,
                'combination' => $this->userCombination,
                'canChange' => $this->userCanChange,
                'cardsToChange' => $this->userCardsToChange
            ],
        ];
        return $return;
    }

    protected function getPayout($isUserWin, $ante, $dealerHandRank, $playerHandRank, $isDealerChengeCard) {
        $bet = $ante * 2;
        if (is_null($isUserWin)) {
            return $ante + $bet;
        }
        if (!$isUserWin) {
            return 0;
        }
        if ($dealerHandRank < 1) {

            return $ante + (!$isDealerChengeCard ? $ante : 0) + $bet;
        }
        $payoutsTable = [
            1 => 1,
            2 => 1,
            3 => 2,
            4 => 3,
            5 => 4,
            6 => 5,
            7 => 7,
            8 => 10,
            9 => 50,
            10 => 100,
        ];
        return $ante + (!$isDealerChengeCard ? $ante : 0) + $bet + $bet * $payoutsTable[$playerHandRank];
    }

    protected function isUserWin(Hand $playerHand, Hand $dealerHand) {
        if ($dealerHand->rank < 1) {
            return true;
        }
        if ($dealerHand->rank > $playerHand->rank) {
            return false;
        }
        if ($dealerHand->rank == $playerHand->rank) {
            foreach ($dealerHand->cardsRanks as $key => $dealerCardRank) {
                $playerCardRank = $playerHand->cardsRanks[$key];
                if ($dealerCardRank == $playerCardRank) {
                    continue;
                }
                return $playerCardRank > $dealerCardRank;
            }
            return null;
        }
        return true;
    }

}
