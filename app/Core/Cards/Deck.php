<?php

namespace App\Core\Cards;

use App\Core\Cards\Card;

class Deck {

    protected $faces = [
        1 => '2',
        2 => '3',
        3 => '4',
        4 => '5',
        5 => '6',
        6 => '7',
        7 => '8',
        8 => '9',
        9 => 'T', //10
        10 => 'J',
        11 => 'Q',
        12 => 'K',
        13 => 'A'
    ];
    protected $suits = [
        1 => 'H', //Hearts
        2 => 'D', //Diamonds
        3 => 'C', //Clubs
        4 => 'S'//Spades
    ];
    protected $deck = [];

    public function __construct() {
        
    }

    public function makeDeck() {
        $deck = [];
        foreach ($this->suits as $suit) {
            foreach ($this->faces as $face) {
                $card = $face . $suit;
                array_push($deck, $card);
            }
        }
        $this->deck = $deck;
    }

    public function shuffle() {
        shuffle($this->deck);
    }

    public function take($cnt) {
        $cards = array_slice($this->deck, 0, $cnt);
        array_splice($this->deck, 0, $cnt);
        return $cnt == 1 ? $cards[0] : $cards;
    }

    public function getDeck() {
        return $this->deck;
    }

    public function setDeck(array $cards) {
        $this->deck = $cards;
    }

}
