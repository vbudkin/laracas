<?php

namespace App\Core\Cards;

class Hand {

    public $cards;
    public $rank = 0;
    public $combination = 'HighCard';
    public $faces = [];
    public $suits = [];
    public $cardsRanks = [];
    protected $facesRanks = [
        '2' => 2,
        '3' => 3,
        '4' => 4,
        '5' => 5,
        '6' => 6,
        '7' => 7,
        '8' => 8,
        '9' => 9,
        'T' => 10,
        'J' => 11,
        'Q' => 12,
        'K' => 13,
        'A' => 14
    ];
    protected $suitsRanks = [
        'H' => 1, //Hearts
        'D' => 2, //Diamonds
        'C' => 3, //Clubs
        'S' => 4//Spades
    ];

    public function __construct(array $cards) {
        $this->cards = $cards;
        $this->parse();
    }

    public function orderCards($asc = true) {
        $cards = [];
        foreach ($this->cards as $card) {
            $key = $this->facesRanks[$card[0]] * 10 + $this->suitsRanks[$card[1]];
            $cards[$key] = $card;
        }
        $asc ? ksort($cards) : krsort($cards);
        $this->cards = array_values($cards);
    }

    protected function parse() {
        foreach ($this->cards as $card) {
            array_push($this->faces, $card[0]);
            array_push($this->suits, $card[1]);
        }
        $this->suits = array_count_values($this->suits);
        $this->faces = array_count_values($this->faces);
        $this->parseRank();
        $this->parseCardRanks();
    }

    protected function parseRank() {
        if ($this->isRoyalFlush()) {
            $this->combination = $this->getCombinationString('RoyalFlush');
            $this->rank = 10;
            return;
        }
        if ($this->isStraightFlush()) {
            $this->combination = $this->getCombinationString('StraightFlush');
            $this->rank = 9;
            return;
        }
        if ($this->isFourOfAKind()) {
            $this->combination = $this->getCombinationString('FourOfAKind');
            $this->rank = 8;
            return;
        }
        if ($this->isFullHouse()) {
            $this->combination = $this->getCombinationString('FullHouse');
            $this->rank = 7;
            return;
        }
        if ($this->isFlush()) {
            $this->combination = $this->getCombinationString('Flush');
            $this->rank = 6;
            return;
        }
        if ($this->isStraight()) {
            $this->combination = $this->getCombinationString('Straight');
            $this->rank = 5;
            return;
        }
        if ($this->isTreeOfAKind()) {
            $this->combination = $this->getCombinationString('TreeOfAKind');
            $this->rank = 4;
            return;
        }
        if ($this->isTwoPairs()) {
            $this->combination = $this->getCombinationString('TwoPairs');
            $this->rank = 3;
            return;
        }
        if ($this->isOnePair()) {
            $this->combination = $this->getCombinationString('OnePair');
            $this->rank = 2;
            return;
        }
        if ($this->isAceKing()) {
            $this->combination = $this->getCombinationString('AceKing');
            $this->rank = 1;
            return;
        }
    }

    protected function getCombinationString($combination) {
        if (in_array($combination, ['HighCard', 'AceKing'])) {
            return $combination;
        }
        $combinationArray[0] = $combination;
        if (in_array($combination, ['OnePair', 'TwoPairs', 'TreeOfAKind', 'FullHouse', 'FourOfAKind'])) {
            foreach ($this->faces as $face => $cnt) {
                if ($cnt > 1) {
                    array_push($combinationArray, str_repeat($face, $cnt));
                }
            }
        }
        if (in_array($combination, ['RoyalFlush', 'StraightFlush', 'Straight'])) {
            $faces = [];
            foreach ($this->faces as $face => $cnt) {
                $faces[$this->facesRanks[$face]] = [
                    'face' => $face,
                    'cnt' => $cnt
                ];
            }
            ksort($faces);
            $sortedFaces = [];
            foreach ($faces as $value) {
                $sortedFaces[$value['face']] = $value['cnt'];
            }
            $c = array_key_first($sortedFaces) . '-' . array_key_last($sortedFaces);
            array_push($combinationArray, $c);
        }
        if (in_array($combination, ['RoyalFlush', 'StraightFlush', 'Flush'])) {
            $suitKey = array_key_first($this->suits);
            $map = [
                'H' => 'Hearts',
                'D' => 'Diamonds',
                'C' => 'Clubs',
                'S' => 'Spades'
            ];
            array_push($combinationArray, $map[$suitKey]);
        }
        return implode(' ', $combinationArray);
    }

    protected function parseCardRanks() {
        $ranks = [];
        foreach ($this->faces as $face => $cnt) {
            $rank = $cnt > 1 ? $this->facesRanks[$face] * 10 * $cnt : $this->facesRanks[$face];
            array_push($ranks, $rank);
        }
        rsort($ranks);
        $this->cardsRanks = $ranks;
    }

    protected function isAceKing() {
        return (in_array('A', array_keys($this->faces)) && in_array('K', array_keys($this->faces)));
    }

    protected function isOnePair() {
        return (array_search(2, $this->faces) != false);
    }

    protected function isTwoPairs() {
        $pairFacesCnt = 0;
        foreach ($this->faces as $value) {
            if ($value == 2) {
                $pairFacesCnt++;
            }
        }
        return ($pairFacesCnt == 2);
    }

    protected function isTreeOfAKind() {
        return (array_search(3, $this->faces) != false);
    }

    protected function isStraight() {
        if (count($this->faces) == 5) {
            $straights = [
                ['A', '2', '3', '4', '5'],
                ['2', '3', '4', '5', '6'],
                ['3', '4', '5', '6', '7'],
                ['4', '5', '6', '7', '8'],
                ['5', '6', '7', '8', '9'],
                ['6', '7', '8', '9', 'T'],
                ['7', '8', '9', 'T', 'J'],
                ['8', '9', 'T', 'J', 'Q'],
                ['9', 'T', 'J', 'Q', 'K'],
                ['T', 'J', 'Q', 'K', 'A']
            ];
            foreach ($straights as $value) {
                if (count(array_intersect($value, array_keys($this->faces))) == 5) {
                    return true;
                }
            }
        }
        return false;
    }

    protected function isFlush() {
        return (array_search(5, $this->suits) != false);
    }

    protected function isFullHouse() {
        return (array_search(2, $this->faces) != false && array_search(3, $this->faces) != false);
    }

    protected function isFourOfAKind() {
        return (array_search(4, $this->faces) != false);
    }

    protected function isStraightFlush() {
        return $this->isFlush() && $this->isStraight();
    }

    protected function isRoyalFlush() {
        $combinations = [
            ['AH', 'KH', 'QH', 'JH', 'TH'],
            ['AD', 'KD', 'QD', 'JD', 'TD'],
            ['AS', 'KS', 'QS', 'JS', 'TS'],
            ['AC', 'KC', 'QC', 'JC', 'TC'],
        ];
        foreach ($combinations as $value) {
            if (count(array_intersect($value, $this->cards)) === 5) {
                return true;
            }
        }
        return false;
    }

}
