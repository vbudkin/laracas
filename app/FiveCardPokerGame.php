<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FiveCardPokerGame extends Model {

    protected $casts = [
        'user_cards' => 'array',
        'dealer_cards' => 'array',
        'cards_left' => 'array',
        'user_changed_cards' => 'array',
        'dealer_changed_cards' => 'array',
    ];
    protected $fillable = [
        'user_id', 'user_cards', 'dealer_cards', 'cards_left', 'ante', 'combination'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function scopeUnfinished($query) {
        return $query->whereNull('isPayout');
    }

}
