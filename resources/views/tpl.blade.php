@extends('layouts.app')

@section('body-class','game')

@section('content')
<div class="container">
    <div class="dealer-bar">
	
    </div>
    <div class="player-bar">

    </div>
</div>
<div class="options-bar fixed-bottom">
    <div class="balance-bar">
	<div class="container">
	    <div class="row">
		<div class="col-sm-12">
		    Balance: 10000 Total bet: 150 Win: 500
		</div>
	    </div>
	</div>
    </div>
    <nav class="navbar navbar-light bg-light align-content-center">
	<div class="container">
	    <button class="btn btn-outline-danger float-left">Drop</button>
	    <button class="btn btn-outline-info">Change</button>
	    <button class="btn btn-outline-success float-right">Bet</button>
	</div>
    </nav>
</div>
@endsection
