@extends('layouts.app')

@section('body-class','game')

@section('content')

<div id="pok" v-cloak>
    <div class="container">
	<div class="row justify-content-center dealer-bar align-items-center">
	    <div class="col-sm-12 col-lg-5 col-md-8">
		<template v-if="gameData.stage !== 'ante'">
		    <div class="hand clearfix">
			<div class="pcard" v-for="(value, key) in gameData.dealer.cards" v-bind:class="getCardClasses(value)"></div>
		    </div>
		    <p class="combination">@{{gameData.dealer.combination ? gameData.dealer.combination : '&nbsp'}}</p>
		</template>
	    </div>
	</div>
	<div class="row justify-content-center align-items-center player-bar">
	    <div class="col-sm-12 col-lg-5 col-md-8">
		<template v-if="gameData.stage !== 'ante'">
		    <div class="hand clearfix">
			<div class="pcard" v-for="(value, key) in gameData.user.cards" v-bind:class="getCardClasses(value,true)" v-on:click="storeChangeCard(value)"></div>
		    </div>
		    <p class="combination">@{{gameData.user.combination}}</p>
		</template>
	    </div>
	</div>
    </div>
    <div class="options-bar fixed-bottom">
	<div class="balance-bar">
	    <div class="container">
		<div class="row">
		    <div class="col-sm-12" style="text-align: center">
			Balance: @{{gameData.balance}} Total bet: @{{gameData.totalBet}} Win: @{{gameData.payout}}
		    </div>
		</div>
	    </div>
	</div>
	<nav class="navbar navbar-light bg-light">
	    <div class="container justify-content-center">
		<div class="form-inline">
		    <template v-if="gameData.stage === 'ante'">
			<div class="input-group">
			    <input type="number" step="5" min="5" max="500" v-model="gameData.ante" class="form-control" v-bind:disabled="isLoading">
			    <div class="input-group-append">
				<button class="btn btn-outline-success" type="button" v-on:click='newGame()' v-bind:disabled="isLoading">
				    <span v-if="isLoading" class="spinner-border spinner-border-sm"></span>
				    <span v-else>Ante</span>
				</button>
			    </div>
			</div>
		    </template>
		    <template v-if="gameData.stage === 'bet'">
			<button class="btn btn-outline-danger" type="button" v-on:click='drop()' v-bind:disabled="isLoading">
			    <span v-if="isLoading" class="spinner-border spinner-border-sm"></span>
			    <span v-else>Drop</span>
			</button>
			<button class="btn btn-outline-success" type="button" v-on:click='makeBet()' v-bind:disabled="isLoading">
			    <span v-if="isLoading" class="spinner-border spinner-border-sm"></span>
			    <span v-else>Bet</span>
			</button>
			<button class="btn btn-outline-info" type="button" v-on:click='changeCards()' v-if="cardsToChange.length > 0" v-bind:disabled="isLoading">Change</button>
		    </template>
		    <template v-if="gameData.stage === 'payout'">
			<template v-if="gameData.payout > 0">
			    <button class="btn btn-outline-success" type="button" v-on:click='makePayout()' v-bind:disabled="isLoading">
				<span v-if="isLoading" class="spinner-border spinner-border-sm"></span>
				<span v-else>Take a win</span>
			    </button>
			    <button class="btn btn-outline-info" type="button" v-on:click='changeDealerCard()' v-if="gameData.dealer.canChange" v-bind:disabled="isLoading">
				<span v-if="isLoading" class="spinner-border spinner-border-sm"></span>
				<span v-else>Buy dealer card</span>
			    </button>
			</template>
			<template v-else>
			    <button class="btn btn-outline-danger" type="button" v-on:click='makePayout()' v-bind:disabled="isLoading">
				<span v-if="isLoading" class="spinner-border spinner-border-sm"></span>
				<span v-else>Drop</span>
			    </button>
			</template>
		    </template>
		</div>
	    </div>
	</nav>
    </div>
</div>
@endsection

@section('footer.scripts')
<script src="{{ asset('js/five-card-poker.js') }}" defer></script>
@endsection