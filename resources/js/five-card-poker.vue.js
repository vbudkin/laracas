import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/index.css';
Vue.use(VueToast, {
  position: 'bottom-right',
  duration: 5000
});
const app = new Vue({
    el: '#pok',
    data: {
        gameData: {
	    ante:5,
	    stage:'ante',
	    id:null,
	    totalBet:null,
	    balance:null,
	    payout:null,
	    dealer:{
		cards:[],
		combination:null,
		canChange:false
	    },
	     user:{
		cards:[],
		combination:null,
		canChange:false,
		cardsToChange:[]
	    }
	    
	},
        dataLoaded: false,
        cardsToChange: [],
        errors: {},
        apiLink: '/api/games/five-card-poker/',
        isLoading: false
    },
    methods: {
        storeChangeCard: function (card) {
            if(!this.gameData.user.canChange){
                return;
            }
            if (_.includes(this.cardsToChange, card)) {
                this.$delete(this.cardsToChange, this.cardsToChange.indexOf(card));
            } else {
                if (this.cardsToChange.length > 1) {
                    return;
                }
                this.cardsToChange.push(card);
            }
        },
        handleHttpErrors: function (error) {
            if (error.response.status === 422) {
		_.forEach(error.response.data.errors, function(value) {
		    _.forEach(value, function(v) {
			Vue.$toast.error(v);
		    });
		});
                this.errors = error.response.data.errors;
            }
	    if (error.response.status === 404) {
		Vue.$toast.error('Game is finished of not found. Please reload page');
	    }
	    if (error.response.status === 500) {
		Vue.$toast.error('Sorry, we have server problems. Try again later');
	    }
            this.isLoading = false;
        },
        handleHttpResponse: function (response) {
            this.gameData = Object.assign({}, this.gameData, response.data);
            this.cardsToChange = [];
            this.isLoading = false;
        },
        beforeHttpRequest: function () {
            this.errors = {};
            this.isLoading = true;
        },
        getGameData: function () {
	    
            var self = this;
            self.beforeHttpRequest();
            axios.get(self.apiLink).then((response) => {
                self.handleHttpResponse(response);
                self.dataLoaded = true;
            }).catch((error) => {
                self.handleHttpErrors(error);
            });
        },
        newGame: function () {
            var self = this;
            self.beforeHttpRequest();
            axios.post('/api/games/five-card-poker', {ante: self.gameData.ante}).then((response) => {
                self.handleHttpResponse(response);
            }).catch((error) => {
                self.handleHttpErrors(error);
            });
        },
        makeBet: function () {
            var self = this;
            self.beforeHttpRequest();
            axios.put(self.apiLink + self.gameData.id + '/bet').then((response) => {
                self.handleHttpResponse(response);
            }).catch((error) => {
                self.handleHttpErrors(error);
            });
        },
        makePayout: function () {
            var self = this;
            self.beforeHttpRequest();
            axios.put(self.apiLink + self.gameData.id + '/payout').then((response) => {
                self.handleHttpResponse(response);
            }).catch((error) => {
                self.handleHttpErrors(error);
            });
        },
        drop: function () {
            var self = this;
            self.beforeHttpRequest();
            axios.put(self.apiLink + self.gameData.id + '/drop').then((response) => {
                self.handleHttpResponse(response);
            }).catch((error) => {
                self.handleHttpErrors(error);
            });
        },
        changeCards: function () {
            var self = this;
            self.beforeHttpRequest();
            axios.put(self.apiLink + self.gameData.id + '/change', {cards: self.cardsToChange}).then((response) => {
                self.handleHttpResponse(response);
            }).catch((error) => {
                self.handleHttpErrors(error);
            });
        },
        changeDealerCard: function () {
            var self = this;
            self.beforeHttpRequest();
            axios.put(self.apiLink + self.gameData.id + '/change-dealer').then((response) => {
                self.handleHttpResponse(response);
            }).catch((error) => {
                self.handleHttpErrors(error);
            });
        },
        getCardClasses: function (card, forUser = false) {
            var result = [];
            if (card !== '') {
                var face = 'face-' + card.charAt(0);
                var suit = 'suit-' + card.charAt(1);
                result.push(face);
                result.push(suit);
                if (forUser && this.gameData.user.canChange) {
                    if (_.includes(this.cardsToChange, card)) {
                        result.push('selected');
                    } else if (this.cardsToChange.length <= 1) {
                        result.push('changeable');
                    }
                }

            } else {
                result.push('card-back');
            }
            return result;
        }
    },
    mounted: function () {
        this.getGameData();
    }
});