<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});



Route::group(['prefix' => 'games/five-card-poker', 'middleware' => 'auth:api'], function() {
    Route::get('/', 'Games\FiveCardPokerApiController@index')->name('five-card-poker-api');
    Route::post('/', 'Games\FiveCardPokerApiController@ante');
    Route::put('{id}/{action}', ['uses' => 'Games\FiveCardPokerApiController@action'])->where('action', 'payout|bet|drop|change|change-dealer');
});

